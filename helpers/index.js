function generateArray(size, min = 0, max = 100) {
  const gen = [];
  for (let i = 0; i < size; ++i) {
    gen.push(Math.floor(min + Math.random() * (max - min + 1)));
  }
  return gen;
}

function compareArrays(arr1, arr2) {
  if (arr1.length !== arr2.length) {
    throw new Error("Arrays aren't same length");
  }

  arr1.forEach((el, idx) => {
    if (el !== arr2[idx]) {
      throw new Error("Arrays aren't equal");
    }
  });

  return true;
}

export function testSort(sort) {
  const arr = generateArray(Math.floor(Math.random() * 10));
  const nativeSorted = arr.slice().sort((a, b) => a - b);
  const customSorted = sort(arr.slice());
  console.log(compareArrays(nativeSorted, customSorted));
}
