import { bubble } from "./algoritms/bubble.js";
import { qsort } from "./algoritms/quick.js";
import { testSort } from "./helpers/index.js";

document
  .getElementById("btn--bubble")
  .addEventListener("click", () => testSort(bubble));

document
  .getElementById("btn--quick")
  .addEventListener("click", () => testSort(qsort));
