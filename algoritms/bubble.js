// O(n^2)
export function bubble(initial) {
  const arr = initial.slice();
  let swapped = false;
  for (let i = 0; i < arr.length - 1; ++i) {
    for (let j = 0; j < arr.length - i - 1; ++j) {
      if (arr[j] > arr[j + 1]) {
        swapped = true;
        arr[j] += arr[j + 1];
        arr[j + 1] = arr[j] - arr[j + 1];
        arr[j] = arr[j] - arr[j + 1];
      }
    }
    if (!swapped) {
      break;
    }
  }
  return arr;
}
