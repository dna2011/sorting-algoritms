/* Quicksort (Hoar sorting alg) JS implementation
 * Time complexity: worst: n^2, medium: n * logn,
 * best: n * logn / n (split for 3 parts)
 * Memory: O(n)
 */
function partition(arr, low, hi) {
  const base = arr[Math.floor(Math.random() * arr.length)];

  while (low <= hi) {
    while (arr[hi] > base) {
      hi--;
    }

    while (arr[low] < base) {
      low++;
    }

    if (low <= hi) {
      [arr[hi], arr[low]] = [arr[low], arr[hi]];
      hi--;
      low++;
    }
  }
  return low;
}

export function qsort(arr, low = 0, hi = arr.length - 1) {
  if (arr.length > 1) {
    const index = partition(arr, low, hi);
    if (low < index - 1) {
      qsort(arr, low, index - 1);
    }
    if (index < hi) {
      qsort(arr, index, hi);
    }
  }
  return arr;
}
